FROM registry.cn-shenzhen.aliyuncs.com/docker_kyle/php-nginx:7.1

#
ENV VIRTUAL_HOST phpinfo.kylel.club

COPY . /app
COPY ./nginx.conf /etc/nginx/conf.d/default.conf


